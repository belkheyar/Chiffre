import java.io.File;
import java.io.IOException;
import java.util.Scanner;

class Chiffre{

    public static void chiffre(File f, String langue, boolean chiffre){
    Scanner sc = new Scanner(System.in);
	String clef;
	String type;
	System.out.println("Entrer le type de code à utiliser |Cesar|Permutation|Vigener|");
	type = sc.nextLine().trim();
	System.out.println("Entrer la clef ou permutation");
	clef = sc.nextLine().trim();
	try{
	    switch(type){
	    case "Cesar":
		chiffre_dechiffre('c', clef, f, chiffre);
		break;
	    case "Permutation":
		chiffre_dechiffre('p', clef, f, chiffre);
		break;
	    default:
		chiffre_dechiffre('v', clef, f, chiffre);
	    }
	}catch(IOException e){
	    e.printStackTrace();
	}
    }
    
    public static void chiffre_dechiffre(char methode, String clef, File f, boolean chiffre)
	throws IOException{
	switch(methode){
	case 'c':
	    ChiffreCesar cc = new ChiffreCesar(Integer.parseInt(clef));
	    if(chiffre) chiffre(cc,f);
	    else dechiffre(cc,f);
	    break;
	case 'p':
	    String[] t = clef.split(",");
	    int[] perm = new int[t.length];

	    for(int i=0; i<t.length; i++)
		perm[i] = Integer.parseInt(t[i]);

	    PermutationChiffre pc = new PermutationChiffre(perm);
	    if(chiffre) chiffre(pc,f);
	    else dechiffre(pc, f);
	    break;
	default:
	    Vigener v = new Vigener(clef);
	    if(chiffre) chiffre(v,f);
	    else dechiffre(v, f);
	}
    }
    
    private static void dechiffre(InterfaceChiffre chiffre_methode, File f)
	throws IOException{
	chiffre_methode.dechiffrer(f);
    }

    private static void chiffre(InterfaceChiffre chiffre_methode, File f)
	throws IOException{
	chiffre_methode.chiffrer(f);
    }
}
