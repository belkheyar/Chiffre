import java.io.File;
import java.io.IOException;

class Dechiffre{
    public static void dechiffre(char methode, String clef, File f)
	throws IOException{
	switch(methode){
	case 'c':
	    ChiffreCesar cc = new ChiffreCesar(Integer.parseInt(clef));
	    dechiffre(cc,f);
	    break;
	case 'p':
	    String[] t = clef.split(",");
	    int[] perm = new int[t.length];

	    for(int i=0; i<t.length; i++)
		perm[i] = Integer.parseInt(t[i]);

	    PermutationChiffre pc = new PermutationChiffre(perm);
	    dechiffre(pc,f);
	    break;
	default:
	    Vigener v = new Vigener(clef);
	    dechiffre(v,f);
	}
    }
    
    private static void dechiffre(InterfaceChiffre chiffre_methode, File f)
	throws IOException{
	chiffre_methode.chiffrer(f);
    }
}
