import java.lang.*;
import java.util.*;
import java.io.*;


public class DecrypteVigener{
    
    public static StringBuffer[] SousText(String texte,int taille){
	StringBuffer[] partie=new StringBuffer[taille];
	int j=0;
	for(int i=0;i<taille;i++){
	    partie[i]=new StringBuffer();
	}
	for(int i=0;i<texte.length();i++){
	    if(DecrypteCesar.charToInt(texte.charAt(i))>=0 && DecrypteCesar.charToInt(texte.charAt(i))<26){
		partie[j].append(texte.charAt(i));
		j=(j+1)%taille;
	    }else{
		partie[j].append(texte.charAt(i));
	    }
	}
	return partie;
    }
    
    public static int Taille_Clef(File f,String langue)throws IOException{
	String texte=DecrypteCesar.getText(f);
	//regarder la longueur qui donne l'id de coin le plus proche du francais
	//ou de l'anglais
	//IC francais 0.0778
	//IC anglais 0.0667
	double IC=0.0;
	if(langue.equals("A")) IC=0.0667;
	else IC= 0.0778;
	double min=1000.0;
        int taille=0;
	for(int i=1;i<30;i++){
	    double[][] freq=new double[i][26];
	    double[] id=new double[i];
	    StringBuffer[] sous_texte=SousText(texte,i);
	    int[] nb_char=new int[i];
	    int nb_tot=0;
	    for(int j=0;j<i;j++){
		nb_char[j]=DecrypteCesar.add_frequ(sous_texte[j].toString(),freq[j]);
		nb_tot+=nb_char[j];
	    }
	    for(int j=0;j<i;j++){
		id[j]=DecrypteCesar.IndiCoi(freq[j],nb_char[j],langue);
	    }
	    double moyenne=0.0;
	    for(int j=0;j<i;j++){
		moyenne+=Math.abs(id[j]-IC);
	    }
	    moyenne=moyenne/i;
	    if(moyenne<min){
		taille=i;
		min=moyenne;
	    }
	}
	return taille;
    }

    
    public static void DecrypteFreq(int taille,File f,String langue)throws IOException{
	//si on veut en anglais il sufit de changer le dico et
	//les frequence des lettre
	Dictionnaire dico;
	int nb_freq=2;
	if(langue.equals("A")){
	    dico=new Dictionnaire("words.txt");
	}else{
	    dico=new Dictionnaire("dico_fr.txt");
	}
	String rep=DecrypteCesar.getText(f);
	StringBuffer[] partie=SousText(rep,taille);
	int[][] clef=new int[taille][nb_freq];
	int t=0;
	for(int i=0;i<taille;i++){
	    t+=partie[i].length();
	}
	for(int i=0;i<taille;i++){
	    clef[i]=DecrypteCesar.getDecalage(partie[i].toString(),langue,nb_freq);
	}
	//faire pour les combinaison de clef les plus probables
	double min=1000.0;
	double[] freq1=new double[26];
	String bon_texte="";
	int[] bonne_clef=new int[taille];
	for(int[] i:clef(taille,clef,new ArrayList<Integer>())){
	    Vigener V=new Vigener(i);
	    String fin=V.dechiffrer(rep);
	    int nbr_char=DecrypteCesar.add_frequ(fin,freq1);
	    for(int j=0;j<26;j++){
		freq1[j]/=nbr_char;
		freq1[j]*=100;
	    }
	    double diff=DecrypteCesar.Carre(freq1,langue);
	    if(diff<min){
		String[] a_verifier=fin.split("[\\s\\p{Punct}]+");
		int mot_correct=0;
		for(String s:a_verifier){
		    if(dico.contains(s)) mot_correct++;
		    if(mot_correct>a_verifier.length/2){
			min=diff;
			bon_texte=fin;
			bonne_clef=i;
			break;
		    }
		}
	    }
	}
        
	System.out.println(bon_texte);
    }

    //renvoi toute les clef possible dans l'ordre des probabité
    //possibilité de faire plus de test a la volé
    public static ArrayList<int[]> clef(int taille,int[][] tab,ArrayList<Integer> list){
	ArrayList<int[]> res=new ArrayList<int[]>();
	if(taille==0){
	    int[] b=new int[list.size()];
	    for(int i=0;i<b.length;i++){
		b[i]=list.get(i);
	    }
	    res.add(b);
	    return res;
	}else{
	    for(int i=0;i<tab[0].length;i++){
		list.add(tab[tab.length-taille][i]);
		res.addAll(clef(taille-1,tab,list));
		list.remove(list.size()-1);
	    }
	    return res;
	}
    }
    
}
