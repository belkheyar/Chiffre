import java.lang.*;
import java.io.*;

public class PermutationChiffre implements InterfaceChiffre{

    private int[] perm;
    private int[] inv_perm;

    public PermutationChiffre(int[] p){
	perm=p;
	inv_perm=new int[26];
	for(int i=0;i<26;i++){
	    inv_perm[perm[i]]=i;
	}
    }

    public char chiffrer(char c){
	if(Character.isLetter(c)){
	    int a = DecrypteCesar.charToInt(c);
	    if(a>=0 && a<26){
		char res= (char)(perm[a]+97);
		return res;
	    }
	    return c;
	}
	return c;
    }

    public String chiffrer(String s){
	StringBuffer res=new StringBuffer();
	for(int i=0;i<s.length();i++){
	    res.append(chiffrer(s.charAt(i)));
	}
	return res.toString();
    }

     public void chiffrer(File f) throws IOException{
	BufferedReader read=new BufferedReader(new FileReader(f));
	while(true){
	    String ligne=read.readLine();
	     if(ligne==null) break;
	    String res=chiffrer(ligne);
	    System.out.println(res);
	}
    }

    public char dechiffrer(char c){
	if(Character.isLetter(c)){
	    int a=DecrypteCesar.charToInt(c);
	    if(a>=0 && a<26){
		char res=(char)(inv_perm[a]+97);
		return res;
	    }
	    return c;
	}
	return c;
    }
    
    public String dechiffrer(String s){
	StringBuffer res=new StringBuffer();
	for(int i=0;i<s.length();i++){
	    res.append(dechiffrer(s.charAt(i)));
	}
	return res.toString();
    }

    public void dechiffrer(File f) throws IOException{
	BufferedReader read=new BufferedReader(new FileReader(f));
	while(true){
	    String ligne=read.readLine();
	     if(ligne==null) break;
	    String res=dechiffrer(ligne);
	    System.out.println(res);
	}
    }
    
}
