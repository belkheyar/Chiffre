import java.io.*;
import java.lang.*;
import java.util.*;

public class ChiffreCesar implements InterfaceChiffre{

    private int clef;

    public ChiffreCesar(int p){
	this.clef=p;
    }
    
    public char chiffrer(char c){
	if(Character.isLetter(c)){
	    int a = DecrypteCesar.charToInt(c);
	    if(a>=0 && a<26){
		char res=(char)(((a+clef)%26)+97);
		return res;
	    }
	}
	return c;
    } 

    public String chiffrer(String s){
	StringBuffer res=new StringBuffer();
	for(int i=0;i<s.length();i++){
	    res.append(chiffrer(s.charAt(i)));
	}
	return res.toString();
    }

    public void chiffrer(File f) throws IOException{
	BufferedReader read=new BufferedReader(new FileReader(f));
	while(true){
	    String ligne=read.readLine();
	     if(ligne==null) break;
	    String res=chiffrer(ligne);
	    System.out.println(res);
	}
    }

    public char dechiffrer(char c){
	if(Character.isLetter(c)){
	    int a = DecrypteCesar.charToInt(c);
	    if(a>=0 && a<26){
		if((a-clef)<0){
		    char res=(char)(((26+(a-clef))%26)+97);
		    return res;
		}
		char res=(char)(((a-clef)%26)+97);
		return res;
	    }
	}
	return c;
    }

    public String dechiffrer(String s){
	StringBuffer res=new StringBuffer();
	for(int i=0;i<s.length();i++){
	    res.append(dechiffrer(s.charAt(i)));
	}
	return res.toString();
    }

    public void dechiffrer(File f) throws IOException{
	BufferedReader read=new BufferedReader(new FileReader(f));
	while(true){
	    String ligne=read.readLine();
	     if(ligne==null) break;
	    String res=dechiffrer(ligne);
	    System.out.println(res);
	}
    }
    
    
}
