import java.io.File;
import java.io.IOException;

public class ChiffreDechiffre{

	/*
		typeCodage fichier clef chiffre("Chiffre" or "Dechiffre")
	*/
    public static void main(String[] args){
    	if(args.length < 4){
	    	System.err.println("Pas assez d'arguments");
	    	return;
		}
		File f = new File(args[1]);
		if(f == null){
	   		System.err.println("Ne peut pas ouvrir le fichier");
	    	return;
		}
		String clef = args[2];
		boolean chiffre = "Chiffre".equals(args[3]);
		try{
		    switch(args[0]){
			    case "Cesar":
					Chiffre.chiffre_dechiffre('c', clef, f, chiffre);
					break;
			    case "Permutation":
					Chiffre.chiffre_dechiffre('p', clef, f, chiffre);
					break;
			    default:
					Chiffre.chiffre_dechiffre('v', clef, f, chiffre);
		    }
		}catch(IOException e){
		    e.printStackTrace();
		}
    }
}