import java.io.*;
import java.util.*;

public interface InterfaceChiffre{

    public char chiffrer(char c);

    public String chiffrer(String s);

    public void chiffrer(File f) throws IOException;

    public char dechiffrer(char c);

    public String dechiffrer(String s);

    public void dechiffrer(File f)throws IOException;
    
}
