import java.util.*;
import java.io.*;

public class DecryptePerm{
  
    public static void decrypteFreq(File f)throws IOException{
	int[] perm=new int[26];
	String texte=DecrypteCesar.getText(f);
	double[] freqText=new double[26];
	int nb_char=DecrypteCesar.add_frequ(texte,freqText);
	ArrayList<Integer> deja=new ArrayList<Integer>();
	for(int i=0;i<26;i++){
	    freqText[i]=(freqText[i]*100/nb_char);
	}
	for(int i=25;i>-1;i--){
	    int i1=maxi(DecrypteCesar.frequencefr,i);
	    int i2=maxi(freqText,i,deja);
	    System.out.println(i2+" "+i1);
	    perm[i1]=i2;
	    deja.add(i2);
	}
	for(int i=0;i<26;i++){
	    System.out.print(perm[i]+" ");
	}
	System.out.println();
	for(int i=0;i<26;i++){
	    System.out.print(DecrypteCesar.frequencefr[i]+" ");
	}
	System.out.println();
	for(int i=0;i<26;i++){
	    System.out.print(freqText[i]+" ");
	}System.out.println();
	PermutationChiffre P=new PermutationChiffre(perm);
	String res=P.dechiffrer(texte);
	//un fois que l'on a la permutation on effectue des transposition sur
	//les lettre qui on frequence 0 jusqu a trouver le bon text avec des mot du dico
	System.out.println(res);
	// le resultat obtenu n'est pas satifaisant si les frequances
	// ne sont pas ordonner de la meme facon que en moyenne
	// il faudrais donc une sorte de regression vers la bonne
	//solution
    }

    public static int maxi(double[] t,int i){
	double[] res=new double[26];
	for(int j=25;j>-1;j--){
	    res[j]=t[j];
	}
	Arrays.sort(res);
	for(int j=25;j>-1;j--){
	    if(res[i]==t[j]) return j;
	}
	return -1;
    }

    public static int maxi(double[] t,int i,ArrayList<Integer> liste){
	double[] res=new double[26];
	for(int j=0;j<26;j++){
	    res[j]=t[j];
	}
	Arrays.sort(res);
	for(int j=25;j>-1;j--){
	    if(res[i]==t[j]){
		if(liste.contains(j)){
		    continue;
		}else{
		return j;
		}
	    }
	}
	return -1;
    }

    public static int[] permAlea(){
	ArrayList<Integer> nbr=new ArrayList<Integer>();
	for(int i=0;i<26;i++){
	    nbr.add(i);
	}
	int[] perm=new int[26];
	for(int i=0;i<26;i++){
	    int n=(int)(Math.random()*(nbr.size()));
	    perm[i]=nbr.get(n);
	    nbr.remove(n);
	}
	return perm;
    }
    

    
    
    
    
    
    
    
    
}
