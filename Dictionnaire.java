import java.io.*;
import java.util.HashSet;

class Dictionnaire extends HashSet<String>{
    
    public Dictionnaire(String filename){
	super();
	String s = "";
	
	//try with ressources
	try(BufferedReader br =
	    new BufferedReader(new FileReader(filename))){
	    while(s != null){
		s = br.readLine();
		this.add(s);
	    }
	}catch(IOException e){ e.printStackTrace(); }
    }
}
