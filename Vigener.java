import java.lang.*;
import java.io.*;

public class Vigener implements InterfaceChiffre{

    private String clef;
    private int[] clef_int;
    private int pos_chiffre=0;
    private int pos_dechiffre=0;
    
    
    public Vigener(String clef){
	this.clef=clef;
	clef_int=new int[clef.length()];
	for(int i=0;i<clef.length();i++){
	    clef_int[i]=(Character.toLowerCase(clef.charAt(i))+'0')-145;
	}
    }

    public Vigener(int[] clef){
	this.clef_int=clef;
    }

    public char chiffrer(char c){
	if(Character.isLetter(c)){
	    int a=(Character.toLowerCase(c)+'0')-145;
	    if(a>=0 && a<26){
		char res=(char)( ((clef_int[pos_chiffre]+a)%26)+97);
		pos_chiffre=(pos_chiffre+1)%clef_int.length;
		return res;
	    }else{
		pos_chiffre=(pos_chiffre+1)%clef_int.length;
		return c;
	    }
	}
	return c;
    }

    public String chiffrer(String s){
	StringBuffer res=new StringBuffer();
	for(int i=0;i<s.length();i++){
	    res.append(chiffrer(s.charAt(i)));
	}
	return res.toString();
    }

     public void chiffrer(File f) throws IOException{
	BufferedReader read=new BufferedReader(new FileReader(f));
	while(true){
	    String ligne=read.readLine();
	     if(ligne==null) break;
	    String res=chiffrer(ligne);
	    System.out.println(res);
	}
    }
    
    public char dechiffrer(char c){
	if(Character.isLetter(c)){
	    int a=(Character.toLowerCase(c)+'0')-145;
	    if(a>=0 && a<26){
		if((a-clef_int[pos_dechiffre])<0){
		    char res=(char)(((26+(a-clef_int[pos_dechiffre]))%26)+97);
		    pos_dechiffre=(pos_dechiffre+1)%clef_int.length;
		    return res;
		}
		char res=(char)(((a-clef_int[pos_dechiffre])%26)+97);
		pos_dechiffre=(pos_dechiffre+1)%clef_int.length;
		return res;
	    }else{
		pos_dechiffre=(pos_dechiffre+1)%clef_int.length;
		return c;
	    }
	}
	return c;
    }

    public String dechiffrer(String s){
	StringBuffer res=new StringBuffer();
	for(int i=0;i<s.length();i++){
	    res.append(dechiffrer(s.charAt(i)));
	}
	return res.toString();
    }

    public void dechiffrer(File f) throws IOException{
	BufferedReader read=new BufferedReader(new FileReader(f));
	while(true){
	    String ligne=read.readLine();
	     if(ligne==null) break;
	    String res=dechiffrer(ligne);
	    System.out.println(res);
	}
    }
    
}
