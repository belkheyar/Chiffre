all: *.java
	javac $^
	jar -cvfm Projet.jar META-INF/MANIFEST.MF *.class
	jarsigner -keystore examplestore -signedjar sProjet.jar Projet.jar signLegal 

clean:
	rm *~

proper:
	rm *.class *.jar
