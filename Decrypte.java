import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

import java.util.Scanner;

class Decrypte{
    
    public static double[] frequence=
    {8.40,1.06,3.03,4.18,17.26,1.12,1.27,0.92,7.34,0.31,0.05,6.01,2.96,
     7.13,5.26,3.01,0.99,6.55,8.08,7.07,5.74,1.32,0.04,0.45,0.30,0.12};

    public static String getText(File f)throws IOException{
		BufferedReader read=new BufferedReader(new FileReader(f));
		StringBuffer st=new StringBuffer("");
		while(true){
	   		String ligne=read.readLine();
	    	if(ligne==null) break;
	    	st.append(ligne);  
		}
		return st.toString();
    }

    public static void main(String[] args){
    	Scanner sc = new Scanner(System.in);
    	int i;
    	long startTime = 0;
    	String type = "";
    	String strategie;
    	String nom;
    	String mot;
    	String langue;
    	String dOrC;
    	System.out.println("Entrer le nom du fichier:");
    	nom = sc.nextLine().trim();
    	File file = new File(nom);
    	if(file == null){
    		System.err.println("Ouverture du fichier impossible: "+nom);
    		return;
    	}
	/*
    	System.out.println("Entrer la langue de votre fichier: |F = français|A = anglais|:");
    	langue = sc.nextLine().trim();
	*/
	langue="A";
		System.out.println("Voulez vous décrypter, chiffrer ou dechiffrer ce fichier ? |D|C|DC|");
		dOrC = sc.nextLine().trim();
		if("C".equals(dOrC)){
			Chiffre.chiffre(file,langue, true);
			return;
		} else if("DC".equals(dOrC)){
			Chiffre.chiffre(file,langue, false);
			return;
		}
    	try{
    		System.out.println("Entrer le type de code utiliser: |Cesar|Vigenere|Permutation|");
    		type = sc.nextLine().trim();
	    	if("Cesar".equals(type)){
	    		System.out.println("Entrer la stratégie à adopter: |1|2|3|");
	    		strategie = sc.nextLine().trim();
	    		if("1".equals(strategie)){
	    			System.out.println("Entrer le mot qui appartient au texte:");
	    			mot = sc.nextLine().trim();
	    			startTime = System.currentTimeMillis();
	    			DecrypteCesar.decrypteMot(file, mot);
	    		} else if("2".equals(strategie)){
	    			startTime = System.currentTimeMillis();
	    			DecrypteCesar.decrypteFreq(file, langue);
	    		} else if("3".equals(strategie)){
	    			startTime = System.currentTimeMillis();
	    			DecrypteCesar.decrypteForceBrute(file, langue);
	    		} else{
	    			System.err.println("Stratégie "+strategie+" invalide !");
	    			return;
	    		}
	    	} else if("Vigenere".equals(type)){
	    		System.out.println("Dechiffrer (D) , Connaitre la taile de la clef (C)");
	    		type = sc.nextLine().trim();
	    		if("D".equals(type)){
	    			System.out.println("Entrer la taille de la clef ou appuyer sur entrer sinon");
	    			type = sc.nextLine().trim();
	    			if("".equals(type)) i = DecrypteVigener.Taille_Clef(file,langue);
	    			else i = Integer.parseInt(type);
	    			startTime = System.currentTimeMillis();
	    			DecrypteVigener.DecrypteFreq(i,file,langue);
	    			type = "Vigenere";

	    		}else if("C".equals(type)){
	    			startTime = System.currentTimeMillis();
	    			System.out.println(DecrypteVigener.Taille_Clef(file,langue));
	    			type = "Vigenere taille clef";
			}else{
	    			System.err.println("Intruction incorrect: "+type);
	    			return;
	    		}
	    	} else if("Permutation".equals(type)){
	    		startTime = System.currentTimeMillis();
	    		DecryptePerm.decrypteFreq(file);
	    	} else{
	    		System.err.println("Type de codage invalide: "+type);
	  			return;
	    	}

	    } catch(NumberFormatException e){
	    	System.out.println("Taille de clef invalide: "+type);
	    }catch(Exception e){
	    	System.out.println(e);
			e.printStackTrace();
	    }
    	long endTime = System.currentTimeMillis();
		System.err.println("Temps de "+type+":" + (endTime-startTime) + "ms");
    }
}








