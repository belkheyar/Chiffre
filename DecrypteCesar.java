import java.io.*;
import java.lang.*;
import java.util.*;

public class DecrypteCesar{

    static class Paire{
	double frequ;
	int dec;
	public Paire(int i,double d){
	    this.frequ=d;
	    this.dec=i;
	}

	public double getFreq(){
	    return this.frequ;
	}

	public int getDec(){
	    return this.dec;
	}
    }

    public static double[] frequencefr=
        // {7.636,0.901,3.260,3.669,14.715,1.066,0.866,0.737,7.529,0.545,0.049,5.456,2.968,7.095,5.378,3.021,1.362,6.553,7.948,7.244,6.311,1.628,0.114,0.387,0.308,0.136};
	// {8.2,0.92,3.33,4.01,17.35,1.08,1.09,0.93,7.53,0.34,0.06,5.92,2.97,7.17,5.53,2.92,1.04,6.65,7.93,6.99,5.73,1.39,0.03,0.47,0.31,0.1};
         {7.265,0.893,3.029,3.538,13.754,1.018,0.972,0.810,7.027,0.480,0.046,5.438,2.743,6.671,5.095,2.704,1.045,6.216,7.800,6.699,5.697,1.398,0.025,0.411,0.269,0.116};
	// {9.42,1.02,2.64,3.39,15.87,0.95,1.04,0.77,8.41,0.89,0.00,5.34,3.24,7.15,5.14,2.86,1.06,6.46,7.90,7.26,6.24,2.15,0.00,0.30,0.24,0.32};

    public static double[] frequenceang=
    {8.08,1.67,3.18,3.99,12.56,2.17,1.80,5.27,7.24,0.14,0.63,4.04,2.60,7.38,7.47,1.91,0.09,6.42,6.59,9.15,2.79,1.00,1.89,0.21,1.65,0.07};


    //renvoi le texte du fichier f
    public static String getText(File f)throws IOException{
	BufferedReader read=new BufferedReader(new FileReader(f));
	StringBuffer st=new StringBuffer("");
	while(true){
	    String ligne=read.readLine();
	    if(ligne==null) break;
	    st.append(ligne);  
	}
	return st.toString();
    }

    //decrypte un fichier f sachant que le mot "mot" est dans le texte
    public static void decrypteMot(File f,String mot) throws IOException{
	String texte =getText(f);
	for(int i=0;i<26;i++){
	    ChiffreCesar C=new ChiffreCesar(i);
	    String d=C.dechiffrer(texte);
	    String[] spl=d.split("[\\s\\p{Punct}]+");
	    for(String j:spl){
		if(mot.equals(j)){
		    System.out.println(d);
		    return ;
		}
	    }
	}
    }
    //affiche le resultat sur la sortie standard

    //decrypte le texte du fichier f en fonction des frequence des lettre
    //dans l'alphabet
    public static void decrypteFreq(File f,String langue)throws IOException{
	String texte=getText(f);
	String rep="";
	double mini=1000.0;
	for(int j=0;j<26;j++){
	    double[] freq=new double[26];
	    for(int i=0;i<26;i++){
		freq[i]=0.0;
	    }
	    ChiffreCesar C=new ChiffreCesar(j);
	    int nb_tot=0;
	    String res=C.dechiffrer(texte);
	    nb_tot+=add_frequ(res,freq);
	    for(int i=0;i<26;i++){
		freq[i]=(freq[i]/nb_tot)*100;
	    }
	    double cmp=Carre(freq,langue);
	    if(mini>cmp){
		mini=cmp;
	        rep=res;
	    }
	}
	System.out.println(rep);
    }

    //renvoi un tableau des n plus probable décalage possible pour la methode de
    //decryptage par frequence sur le texte "texte"
    public static int[] getDecalage(String texte,String langue,int nb_freq) throws IOException{
	String rep="";
	double mini=1000.0;
	ArrayList<Paire> liste=new ArrayList<Paire>();
	int[] decalage=new int[nb_freq];
	for(int j=0;j<26;j++){
	    double[] freq=new double[26];
	    for(int i=0;i<26;i++){
		freq[i]=0.0;
	    }
	    ChiffreCesar C=new ChiffreCesar(j);
	    int nb_tot=0;
	    String res=C.dechiffrer(texte);
	    nb_tot+=add_frequ(res,freq);
	    for(int i=0;i<26;i++){
		freq[i]=(freq[i]/nb_tot)*100;
	    }
	    double cmp=Carre(freq,langue);
	    decal(liste,j,cmp,nb_freq);
	}
	for(int i=0;i<decalage.length;i++){
	    decalage[i]=liste.get(i).getDec();
	}
	System.gc();
	return decalage;
    }

    //met a jour liste avec les Paire(decalage frequence) de la frequence la
    //proche de l'alphabet choisi a celle la plus eloigner
    public static void decal(ArrayList<Paire> liste,int j,double diff,int nb_freq){
	int nbr_elem=nb_freq;
	//nbr_elem donne la taille de liste et donc le nombre de decalage possible
	Paire p=new Paire(j,diff);
	if(liste.size()==0) liste.add(p);
	else{
	    for(int i=0;i<liste.size();i++){
		if(liste.get(i).getFreq()>diff){
		    liste.add(i,p);
		    return;
		}
	    }
	    liste.add(p);
	}
    }
    
    //Probleme pour lire les textes avec accents
    public static void decrypteForceBrute(File f, String langue)
	throws IOException{
	Dictionnaire dico;

	if(langue.equals("A"))
	    dico = new Dictionnaire("words.txt");
	else
	    dico = new Dictionnaire("dico_fr.txt");
	String rep = "";
	String texte = getText(f);
	int mots_corrects_max = 0;
	int meilleur_decalage = 0;
	for(int i=0; i<26; i++){
	    ChiffreCesar cc = new ChiffreCesar(i);
	    String dec = cc.dechiffrer(texte);
	    String[] tab = dec.split("[\\s\\p{Punct}]+");
	    int mots_corrects = 0;
	    for(String s: tab){
		if(dico.contains(s)){
		    mots_corrects++;
		}
	    }
	    if(mots_corrects > mots_corrects_max){
		meilleur_decalage = i;
		mots_corrects_max = mots_corrects;
	    }
	}
	System.out.println("nb de mots corrects : "+mots_corrects_max);
	System.out.println("meilleur decalage : "+meilleur_decalage);
	ChiffreCesar cc = new ChiffreCesar(meilleur_decalage);
	String str_decrypte = cc.dechiffrer(texte);
	System.out.println(str_decrypte);
    }
    
    public static int charToInt(char c){
	if(Character.isLetter(c)){
	    int a = Character.getNumericValue(Character.toLowerCase(c)) - 10;
	    return a;
	}
	return -1;
    }

    //renvoi la somme des difference entre la frequence de chaque lettre dans
    //le texte etudier et sa frequence dans l'alphabet choisi
    public static double compare(double[] tab,String langue){
	double min=0.0;
	for(int i=0;i<26;i++){
	    double dif;
	    if(langue.equals("A")){
		dif=frequenceang[i]-tab[i];
	    }else{
		dif=frequencefr[i]-tab[i];
	    }
	    double t=Math.abs(dif);
	    min+=t;
	}
	return min;

    }

    public static double IndiCoi(double[] tab,int N,String langue){
	// on prend N le nombre total de lettre
	  int N1=N*(N-1);
	  double tot=0.0;
	  for(int i=0;i<26;i++){
	      tot+=(tab[i]*(tab[i]-1))/N1;
	  }
	  return tot;
	  //  renvoi l'indice de coincidence 
	  //IC francais 0.0778
	  //IC anglais 0.0667
    }

    public static double Carre(double[] tab,String langue){
	if(langue.equals("A")){
	    double C=0.0;
	    for(int i=0;i<26;i++){
		C+=Math.pow(tab[i]-frequenceang[i],2.0)/frequenceang[i];
	    }
	    return C;
	}else{
	    double C=0.0;
	    for(int i=0;i<26;i++){
		C+=Math.pow(tab[i]-frequencefr[i],2.0)/frequencefr[i];
	    }
	    return C;
	}	
    }

    
    //ajoute le nombre d'occurence de chaque lettre de s dans tab
    //nbr de a dans tab[0] ... nbr de z dans tab[25]
    public static int add_frequ(String s,double[] tab){
	int taille=0;
	for(int i=0;i<s.length();i++){
	    char c=s.charAt(i);
	    if(Character.isLetter(c)){
		int a=charToInt(c);
		if(a>=0 && a<26){
		    tab[a]++;
		    taille++;
		}
	    }
        }
	return taille;
    }
    
}
